#django-pagedown-bootstrap

This is custom app which merges timmyomahony's excellent [django-pagedown][1] app with [samwillis' pagedown-bootstrap][2].

**It is not really intended for public use as I have modified the JS/HTML output to fit my project's aesthetic requirements.**

Having said that, if you want to use a Stackoverflow-style markdown editor in a Django environment with Twitter Bootstrap's CSS framework, then you are welcome to use/fork this repo.

I have not made any major changes except to remove the option of a help button, add `<p>Preview</p>` to the output and put the preview pane in a "well" class. There are also a couple of custom CSS classes to add a margin here and there.

For usage, I suggest you have a look at the original packages.

In terms of displaying the markdown output, I recommend the excellent [django-markdown-deux][3]

[1]: https://github.com/timmyomahony/django-pagedown
[2]: https://github.com/samwillis/pagedown-bootstrap
[3]: https://github.com/trentm/django-markdown-deux